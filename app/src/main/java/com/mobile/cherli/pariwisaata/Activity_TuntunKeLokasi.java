package com.mobile.cherli.pariwisaata;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cherly on 24/02/2016.
 */
public class Activity_TuntunKeLokasi extends Activity implements android.location.LocationListener {
    public GoogleMap map;
    ArrayList<LatLng> markerPoints;
    TextView tvJarakWaktu;
    ///GPSTracker gps;
    Button bRefresh;
    ImageButton ibRefresh;

    ProgressDialog pd;


    android.location.LocationListener locationListener;
    //private MyLocationListener locationListener;
    private LocationManager locationManager;
    private String provider;
    private Location lokasi;

    public static final LatLng LOCATION_PADANG = new LatLng(-0.9652668, 100.39675);

    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    // flag for GPS status
    boolean canGetLocation = false;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    private static final String TAG_ID = "id_wisata";
    private static final String TAG_NAMA = "nama_wisata";
    private static final String TAG_LATITUDE = "lat";
    private static final String TAG_LONGITUDE = "long";
    private static final String TAG_KET = "keterangan";
    private static final String TAG_KOTA = "nama_kota";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tuntunlokasi);

        tvJarakWaktu = (TextView) findViewById(R.id.tv_jarak_waktu);
        ibRefresh = (ImageButton) findViewById(R.id.ibRefersh);
        //Tombol Animasi
        //final Animation putar = AnimationUtils.loadAnimation(this, R.anim.putar);
        // Mendapatkan data yang di lempar dari Activity sebelumnya
        Intent gtuntun = getIntent();
        String namawisata = gtuntun.getStringExtra(TAG_NAMA);
        String latitude = gtuntun.getStringExtra(TAG_LATITUDE);
        String longitude = gtuntun.getStringExtra(TAG_LONGITUDE);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_tuntunlokasi)).getMap();
        CameraUpdate dekat = CameraUpdateFactory.newLatLngZoom(LOCATION_PADANG, 10);
        map.animateCamera(dekat);

        //initializing map
        markerPoints = new ArrayList<LatLng>();
        // Untuk mengetahui Posisi kita Icon GPS
        map.setMyLocationEnabled(true);
        //map.setTrafficEnabled(true);
        //Metode Memperoleh Lokasi dari Device
        getLocation();

        LatLng awal = new LatLng(getLatitude(),getLongitude());
        //Mendapatkan latitude longitude dari String yang di lempar dari Activity sebelumnya
        double latwisata = Double.parseDouble(latitude);
        double longwisata = Double.parseDouble(longitude);

        final LatLng wisatalatlon = new LatLng(latwisata, longwisata);
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(wisatalatlon);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        markerOptions.title(namawisata);
        map.addMarker(markerOptions);

        String url = getDirectionsUrl(awal,wisatalatlon);
        DownloadTask downloadTask = new DownloadTask();

        // Memulai download json data dari google direction Api
        downloadTask.execute(url);
//
}

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Route awal
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Route tujuan
        String str_dest = "destination="+dest.latitude+","+dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;


        return url;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(getBaseContext(),  "Lokasi Berubah!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public Location getLocation() {
        try {
            locationManager = (LocationManager) getBaseContext().getSystemService(LOCATION_SERVICE);

            // Mendapatkan status GPS
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);

            // mendapatkan Status Network
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // Jika GPS enable get lat/long menggunakan GPS services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    public double getLatitude() {
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude() {
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }

    /** A method to download json data dari url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // membuat http connection untuk komunikasi dengan url
            urlConnection = (HttpURLConnection) url.openConnection();

            // koneksi dengan url
            urlConnection.connect();

            // Membaca data dari url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private class DownloadTask extends AsyncTask<String, Void, String> {

        protected void onPreExecute () {
            super.onPreExecute();
            pd = new ProgressDialog(Activity_TuntunKeLokasi.this);
            pd.setTitle("Processing");
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();

        }

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            pd.dismiss();

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
        }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> > {
        public void execute(String result) {
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionJSONParser parser = new DirectionJSONParser();

                // Memulai parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }
        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            pd.dismiss();


            if(result.size()<1){
                Toast.makeText(getBaseContext(), "No Points, Aktifkan GPS", Toast.LENGTH_SHORT).show();
                return;
            }


            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){  // Get Mendapatkan Jarak dari list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get Waktu Dari list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(4);
                lineOptions.color(Color.GREEN);

            }

            tvJarakWaktu.setText("Jarak: "+distance + ", Waktu: "+duration);
            //tvJarakWaktu.setTextColor(Color.parseColor("#FFFFFF"));
            // Drawing polyline in the Google Map for the i-th route
            map.addPolyline(lineOptions);
        }
    }
}

