package com.mobile.cherli.pariwisaata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Cherly on 24/02/2016.
 */
public class Activity_PencarianDetail extends Activity {


    private static final String TAG_ID="id_wisata";
    private static final String TAG_NAMA="nama_pariwisata";
    private static final String TAG_LATITUDE="latitude";
    private static final String TAG_LONGITUDE="longtitude";
    private static final String TAG_KET="keterangan";
    private static final String TAG_KOTA="nama_kota";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pencarian_detail);

        Button tuntunLokasi=(Button)findViewById(R.id.bTuntunkLokasi);
        TextView tvNamaWisata=(TextView)findViewById(R.id.txtNama);
        TextView tvLat=(TextView)findViewById(R.id.txtlat);
        TextView tvlong=(TextView)findViewById(R.id.txtlong);
        TextView tvKet=(TextView)findViewById(R.id.txtket);

        //Mengambil atau mendapatkan data yang telah di lempar dari Activity_Pencarian
        Intent i = getIntent();
        final String namawisata = i.getStringExtra(TAG_NAMA);
        final String latitude = i.getStringExtra(TAG_LATITUDE);
        final String longitude = i.getStringExtra(TAG_LONGITUDE);
        String ket=i.getStringExtra(TAG_KET);

        tvNamaWisata.setText(namawisata);
        //tvKota.setText(kota);
        tvLat.setText(latitude);
        tvlong.setText(longitude);
        tvKet.setText(ket);

        tuntunLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tuntun = new Intent(Activity_PencarianDetail.this, Activity_TuntunKeLokasi.class);

                tuntun.putExtra(TAG_NAMA, namawisata);
                tuntun.putExtra(TAG_LATITUDE, latitude);
                tuntun.putExtra(TAG_LONGITUDE, longitude);

                startActivity(tuntun);

              //  Activity_PencarianDetail.this.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }
}
