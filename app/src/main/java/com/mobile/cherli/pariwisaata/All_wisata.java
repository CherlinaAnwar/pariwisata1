package com.mobile.cherli.pariwisaata;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cherly on 29/02/2016.
 */
public class All_wisata extends Activity implements GoogleMap.OnInfoWindowClickListener,GoogleMap.OnMapLongClickListener {
public ImageView imgExample;
    public GoogleMap map;

    public static final LatLng LOCATION_PADANG = new LatLng(-0.9652668, 100.39675);

  /*  private static final String TAG_ID = "id_wisata";
    private static final String TAG_NAMA = "nama_wisata";
    private static final String TAG_LAT = "lat";
    private static final String TAG_LONG = "long";
    private static final String TAG_KET = "keterangan";
    private static final String TAG_KOTA = "nama_kota"; */
    private static final String TAG_IMAGE="image";

    HashMap<String, HashMap> extraMarkerInfo = new HashMap<String, HashMap>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.peta);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_coba)).getMap();
        CameraUpdate dekat = CameraUpdateFactory.newLatLngZoom(LOCATION_PADANG, 10);
        map.animateCamera(dekat);
        // map.setMyLocationEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setInfoWindowAdapter(new MyInfoWindowAdapter());
        map.setOnInfoWindowClickListener(this);
        new RetrieveTask().execute();


    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.tvnamawisata));
            tvTitle.setText(marker.getTitle());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.Snippet));
            tvSnippet.setText(marker.getSnippet());
            imgExample = (ImageView)myContentsView.findViewById(R.id.image1);
            return myContentsView;

        }


    }

    private class RetrieveTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String strUrl = "http://10.0.3.2/pariwisata/lokasi.php";
            URL url = null;
            StringBuffer sb = new StringBuffer();
            try {
                url = new URL(strUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream iStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }


    private class ParserTask extends AsyncTask<String, Void, List<HashMap<String, String>>> {
        @Override
        protected List<HashMap<String, String>> doInBackground(String... params) {
            PariwisataJSOnParser wisataParser = new PariwisataJSOnParser();
            JSONObject json = null;
            try {
                json = new JSONObject(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            List<HashMap<String, String>> wisataList = wisataParser.parse(json);
            return wisataList;
        }

        protected void onPostExecute(List<HashMap<String, String>> result) {
            for (int i = 0; i < result.size(); i++) {
                HashMap<String, String> wisata = result.get(i);
                LatLng latlng = new LatLng(Double.parseDouble(wisata.get("latitude")), Double.parseDouble(wisata.get("longtitude")));

                String nmwisata = new String(wisata.get("nama_pariwisata"));
                String lat = wisata.get("latitude");
                String lon = wisata.get("longtitude");
                String images=wisata.get("image");
                Picasso.with(getApplicationContext()).load(images).into(imgExample);

                //  String idwisata = new String(wisata.get("id_wisata"));
                String ketwisata = new String(wisata.get("keterangan"));
                //String nmkota = new String(wisata.get("nama_kota"));

                System.out.println("Latitude " + lat + " " + "Longtitude " + lon);
                TambahMarker(latlng, nmwisata,ketwisata);
            }
        }
    }

    private void TambahMarker(LatLng latlng,String nmwisata, String keterangan) {
       /* MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latlng);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        markerOptions.title(nmwisata);
        map.addMarker(markerOptions); */
        Marker marker = map.addMarker(new MarkerOptions().position(latlng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(nmwisata).snippet(keterangan));

   /*     HashMap data = new HashMap();
      //  data.put(TAG_ID, idwisata);
        data.put(TAG_NAMA, nmwisata);
        data.put(TAG_LAT, latwisata);
        data.put(TAG_LONG, longwisata);
       // data.put(TAG_KET, ketwisata);
        //data.put(TAG_KOTA, kota);

        extraMarkerInfo.put(marker.getId(),data);

        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // Check if there is an open info window
                HashMap marker_data = extraMarkerInfo.get(marker.getId());

                Intent bukahalamanbaru = new Intent(MainActivity.this, Activity_PencarianDetail.class);

             //   bukahalamanbaru.putExtra(TAG_ID, marker_data.get(TAG_ID).toString());
                bukahalamanbaru.putExtra(TAG_NAMA, marker_data.get(TAG_NAMA).toString());
             //   bukahalamanbaru.putExtra(TAG_KOTA, marker_data.get(TAG_KOTA).toString());
                bukahalamanbaru.putExtra(TAG_LAT, marker_data.get(TAG_LAT).toString());
                bukahalamanbaru.putExtra(TAG_LONG, marker_data.get(TAG_LONG).toString());
            //    bukahalamanbaru.putExtra(TAG_KET, marker_data.get(TAG_KET).toString());
                startActivity(bukahalamanbaru);
                return true;
            }
        }); */


    }
}
