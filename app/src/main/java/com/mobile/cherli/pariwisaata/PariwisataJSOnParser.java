package com.mobile.cherli.pariwisaata;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cherly on 27/02/2016.
 */
public class PariwisataJSOnParser {
    /** Menerima  JSONObject dan mengembalikannya menjadi list */
    public List<HashMap<String,String>> parse(JSONObject jObject){

        JSONArray jPariwisatas = null;
        try{

            jPariwisatas = jObject.getJSONArray("data");
        }catch(JSONException e){
            e.printStackTrace();
        }

        return getPariwisatas(jPariwisatas);
    }

    private List<HashMap<String, String>> getPariwisatas(JSONArray jPariwisatas){
        int pariwisataCount = jPariwisatas.length();
        List<HashMap<String, String>> pariwisataList = new ArrayList<HashMap<String,String>>();
        HashMap<String,String> wisata = null;

        /** mengambil setiap data sekolah, parses dan menambahkan ke list object*/
        for(int i=0; i<pariwisataCount;i++){
            try{
                wisata = getPariwisata((JSONObject)jPariwisatas.get(i));
                pariwisataList.add(wisata);
            }catch(JSONException e){
                e.printStackTrace();
            }
        }
        return pariwisataList;
    }


    private HashMap<String, String> getPariwisata(JSONObject jPariwisata){

        HashMap<String, String> wisata = new HashMap<String, String>();
        String idwisata = "-NA-";
        String namawisata = "-NA-";
        String idklasifikasi = "-NA-";
        String latitude = "-NA-";
        String longitude = "-NA-";
        String keterangan = "-NA-";
        String image= "-NA-";
        try{

            if(!jPariwisata.isNull("id_wisata")){
                idwisata = jPariwisata.getString("id_wisata");
            }if(!jPariwisata.isNull("nama_pariwisata")){ //
                namawisata = jPariwisata.getString("nama_pariwisata");
            }if(!jPariwisata.isNull("keterangan")){
                keterangan= jPariwisata.getString("keterangan");
            }if(!jPariwisata.isNull("id_klasifikasi")){
                idklasifikasi = jPariwisata.getString("id_klasifikasi");
            }if(!jPariwisata.isNull("latitude")){
                latitude = jPariwisata.getString("latitude");
            }if(!jPariwisata.isNull("longtitude")){
                longitude = jPariwisata.getString("longtitude");
            }if(!jPariwisata.isNull("image")){
                image=jPariwisata.getString("image");
            }

            wisata.put("id_wisata", idwisata);
            wisata.put("nama_pariwisata", namawisata);
            wisata.put("keterangan", keterangan);
            wisata.put("id_klasifikasi", idklasifikasi);
            wisata.put("latitude", latitude);
            wisata.put("longtitude", longitude);
            wisata.put("image",image);

        }catch(JSONException e){
            e.printStackTrace();
        }
        return wisata;
    }
}
