package com.mobile.cherli.pariwisaata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
Button btnall,btnklasifikasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnall=(Button)findViewById(R.id.all);
        btnklasifikasi=(Button)findViewById(R.id.kota);

        btnall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this, All_wisata.class);
                startActivity(i);
            }
        });

        btnklasifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,Listkota.class);
                startActivity(i);
            }
        });
    }
}
