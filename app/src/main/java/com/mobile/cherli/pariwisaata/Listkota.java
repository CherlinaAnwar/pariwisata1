package com.mobile.cherli.pariwisaata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Cherly on 29/02/2016.
 */
public class Listkota extends Activity{

    String menu[]={"Payakumbuh dan Kab.50 kota","Bukittinggi dan Kab.Agam","Batusangkar dan Kab.tanah Datar","Pesisir Selatan"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_kota);

        ListView list1=(ListView)findViewById(R.id.list1);
        list1.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,menu));
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i=null;
                if (position==0) {
                i=new Intent(Listkota.this,Klasifikasi1.class);
            }else{ }
               startActivity(i);

    }
    });}

}
